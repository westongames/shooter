// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacter.h"
#include "Gimka/Misc/GKLogger.h"
#include "Gimka/AI/Brain/GKCharacterBrainPlayer.h"
#include "Gimka/Steering/GKCharacterSteering.h"
#include "Gimka/Weapon/GKWeaponManager.h"

#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Camera/CameraComponent.h"

// Sets default values
AShooterCharacter::AShooterCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create a camera boom (pulls in towards the character if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.f;			// The camera follows at this distance behind the character
	CameraBoom->bUsePawnControlRotation = true;		// Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);		// Attach camera to end of boom
	FollowCamera->bUsePawnControlRotation = false;		// Camera does not rotate relative to arm

	// Don't rotate when the controller rotates. Let the controller only affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true;	// Character moves in the direction of input...
	GetCharacterMovement()->RotationRate = FRotator(0.f, 540.f, 0.f);	// ... at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create the character controller
	SetCharacterBrain(CreateDefaultSubobject<UGKCharacterBrainPlayer>(TEXT("CharacterBrainPlayer")));

	// Create the character movement
	SetCharacterSteering(CreateDefaultSubobject<UGKCharacterSteering>(TEXT("CharacterSteering")));

	// Create the weapon manager
	SetWeaponManager(CreateDefaultSubobject<UGKWeaponManager>(TEXT("WeaponManager")));

	GKLog("AShooterCharacter done");
}

// Called when the game starts or when spawned
void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	GKLog("BeginPlay() called!");

	int32 MyInt{ 42 };
	//GKDebug::Log("int myInt %d", MyInt);
	GKLog1("myInt", MyInt);

	float MyFloat{ 3.14159f };
	//GKDebug::Log("int myInt %d", MyFloat);
	GKLog1("MyFloat", MyFloat);

	double MyDouble{ 0.000756 };
	//GKDebug::Log("int myInt %d", MyDouble);
	GKLog1("MyDouble", MyDouble);

	char MyChar{'J'};
	//GKDebug::Log("int myInt %d", MyChar);
	GKLog1("MyChar", MyChar);

	wchar_t MyWChar{L'J'};
	//GKDebug::Log("int myInt %d", MyWChar);
	GKLog1("MyWChar", MyWChar);

	bool MyBool{ true };
	//GKDebug::Log("int myInt %d", MyBool);*
	GKLog1("MyBool", MyBool);

	GKLog1("MyFString", FString("Bla"));

	GKLog1("MyChars", "Hell");
	
	GKLog("It's a log instance!");

	//GKLOG_NUM("int myInt", MyInt);

	//GKLOG_NUM("int myInt", 33);
}

// Called every frame
void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

