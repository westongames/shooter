// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
static class SHOOTER_API GKLogger
{
public:

	// Use the GKLog instead
	static void Log(const char* text) { UE_LOG(LogTemp, Warning, TEXT("%s"), *FString(text)); }
	static void Log(const char* text, int32 val) { UE_LOG(LogTemp, Warning, TEXT("%s: %d"), *FString(text), val); }
	static void Log(const char* text, float val) { UE_LOG(LogTemp, Warning, TEXT("%s: %f"), *FString(text), val); }
	static void Log(const char* text, double val) { UE_LOG(LogTemp, Warning, TEXT("%s: %lf"), *FString(text), val); }
	static void Log(const char* text, char val) { UE_LOG(LogTemp, Warning, TEXT("%s: %c"), *FString(text), val); }
	static void Log(const char* text, wchar_t val) { UE_LOG(LogTemp, Warning, TEXT("%s: %lc"), *FString(text), val); }
	static void Log(const char* text, bool val) { UE_LOG(LogTemp, Warning, TEXT("%s: %d"), *FString(text), val); }
	static void Log(const char* text, const char* val) { UE_LOG(LogTemp, Warning, TEXT("%s: %s"), *FString(text), *FString(val)); }
	static void Log(const char* text, const FString& val) { UE_LOG(LogTemp, Warning, TEXT("%s: %s"), *FString(text), *val); }

	static void Log(const char* text, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s"), *instance, line, *FString(text)); }
	static void Log(const char* text, int32 val, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: %d"), *instance, line, *FString(text), val); }
	static void Log(const char* text, float val, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: %f"), *instance, line, *FString(text), val); }
	static void Log(const char* text, double val, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: %lf"), *instance, line, *FString(text), val); }
	static void Log(const char* text, char val, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: %c"), *instance, line, *FString(text), val); }
	static void Log(const char* text, wchar_t val, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: %lc"), *instance, line, *FString(text), val); }
	static void Log(const char* text, bool val, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: %d"), *instance, line, *FString(text), val); }
	static void Log(const char* text, const char* val, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: %s"), *instance, line, *FString(text), *FString(val)); }
	static void Log(const char* text, const FString& val, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: %s"), *instance, line, *FString(text), *val); }

};

// The uppercase version will always log the instance name
#define GKLOG(text) GKLogger::Log(text, GetName(), __LINE__)
#define GKLOG1(text, val) GKLogger::Log(text, val, GetName(), __LINE__)

// The lowercase version will depend on the LOG_INSTANCE_NAME
#define LOG_INSTANCE_NAME
#ifdef LOG_INSTANCE_NAME

	#define GKLog(text) GKLogger::Log(text, GetName(), __LINE__)
	#define GKLog1(text, val) GKLogger::Log(text, val, GetName(), __LINE__)

#else
	#define GKLog(text) GKLogger::Log(text)
	#define GKLog1(text, val) GKLogger::Log(text, val)

#endif
