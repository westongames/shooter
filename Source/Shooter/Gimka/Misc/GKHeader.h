// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
static class SHOOTER_API GKHeader
{
public:

};

#define GKPTR_GET(varType, varName) FORCEINLINE varType* Get ## varName () const { return varName; }

#define GKPTR_SET(varType, varName) FORCEINLINE void Set ## varName (varType* val) { varName = val; }

//#define GKPROPERTY_PTR_GET(varType, varName, category)		\
//	private:											\
//		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = category, meta = (AllowPrivateAccess = "true"))				\
//		class varType* varName;				\
//	public:									\
//		FORCEINLINE varType* Get ## varName () const { return varName; }

//#define GKPROPERTY_PTR_GET(varType, varName, category)		\
//		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = category, meta = (AllowPrivateAccess = "true"))				\
//		class varType* varName;				\
//	public:									\
//		FORCEINLINE varType* Get ## varName () const { return varName; }

#define GKSINGLETON_ACTOR(varType)									\
	public:															\
		static varType* GetInstance()								\
		{															\
			check(Instance);										\
			return Instance;										\
		}															\
	private:														\
		static varType* Instance;