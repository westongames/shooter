// Fill out your copyright notice in the Description page of Project Settings.


#include "GKCharacterBrain.h"
#include "Gimka/Steering/GKCharacterSteering.h"

// Sets default values for this component's properties
UGKCharacterBrain::UGKCharacterBrain()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGKCharacterBrain::BeginPlay()
{
	Super::BeginPlay();

	// The Actor must be a Pawn
	Pawn = Cast<APawn>(GetOwner());
	check(Pawn);

	// Reference to Steering
	Steering = GetOwner()->FindComponentByClass<UGKCharacterSteering>();
	check(Steering);

}


// Called every frame
void UGKCharacterBrain::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

