// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "Gimka/Misc/GKHeader.h"

#include "GKCharacterBrain.generated.h"

/*
* The Brain, can be AI, human player, or autonomous (from server)
*/

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTER_API UGKCharacterBrain : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGKCharacterBrain();

	// Call this in SetupPlayerInputComponent. If it's human player controlled, the keys will be binded.
	virtual void SetupInputComponent(UInputComponent* InputComponent) {}

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UPROPERTY()
	APawn* Pawn;				// Reference to owner Actor, which is a pawn

	UPROPERTY()			//VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	class UGKCharacterSteering* Steering;			// Reference to attached CharacterMovement

protected:
	GKPTR_GET(APawn, Pawn)
	GKPTR_GET(UGKCharacterSteering, Steering)
};
