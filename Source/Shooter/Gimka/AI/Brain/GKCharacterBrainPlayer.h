// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GKCharacterBrain.h"

#include "GKCharacterBrainPlayer.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTER_API UGKCharacterBrainPlayer : public UGKCharacterBrain
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGKCharacterBrainPlayer();

	// Call this in SetupPlayerInputComponent. If it's human player controlled, the keys will be binded.
	virtual void SetupInputComponent(UInputComponent* InputComponent) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	// Called for forwards/backwards input
	void MoveForward(float Value);

	// Called for side to side input
	void MoveRight(float Value);

	/*
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/*
	* Called via input to look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired rate
	*/
	void LookUpAtRate(float Rate);

	/* Called when the Fire Button is pressed */
	void FireWeapon();
};
