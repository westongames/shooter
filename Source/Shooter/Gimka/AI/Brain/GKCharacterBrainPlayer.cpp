// Fill out your copyright notice in the Description page of Project Settings.

#include "GKCharacterBrainPlayer.h"
#include "Gimka/Steering/GKCharacterSteering.h"
#include "Gimka/Misc/GKLogger.h"

#include "GameFramework/Character.h"



// Sets default values for this component's properties
UGKCharacterBrainPlayer::UGKCharacterBrainPlayer()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGKCharacterBrainPlayer::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UGKCharacterBrainPlayer::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UGKCharacterBrainPlayer::SetupInputComponent(UInputComponent* InputComponent)
{
	Super::SetupInputComponent(InputComponent);
	check(InputComponent);

	InputComponent->BindAxis("MoveForward", this, &UGKCharacterBrainPlayer::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &UGKCharacterBrainPlayer::MoveRight);
	InputComponent->BindAxis("TurnRate", this, &UGKCharacterBrainPlayer::TurnAtRate);
	InputComponent->BindAxis("LookUpRate", this, &UGKCharacterBrainPlayer::LookUpAtRate);

	// Note: GetPawn is not valid yet, so get it here
	ACharacter* TheCharacter = Cast<ACharacter>(GetOwner());
	check(TheCharacter);

	// TODO: move this to separate function. Also add ability to invert Y (right now via Project Setting - Input)
	InputComponent->BindAxis("Turn", TheCharacter, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("LookUp", TheCharacter, &APawn::AddControllerPitchInput);

	InputComponent->BindAction("Jump", IE_Pressed, TheCharacter, &ACharacter::Jump);
	InputComponent->BindAction("Jump", IE_Released, TheCharacter, &ACharacter::StopJumping);

	InputComponent->BindAction("FireButton", IE_Pressed, this, &UGKCharacterBrainPlayer::FireWeapon);

}


void UGKCharacterBrainPlayer::MoveForward(float Value)
{
	AController* Controller = GetPawn()->Controller;
	if (Controller != nullptr && Value != 0.0f)
	{
		// Find out which way is forward
		const FRotator& Rotation = Controller->GetControlRotation();
		const FRotator YawRotation{ 0, Rotation.Yaw, 0 };

		const FVector Direction{ FRotationMatrix {YawRotation}.GetUnitAxis(EAxis::X) };
		GetPawn()->AddMovementInput(Direction, Value);
	}
}

void UGKCharacterBrainPlayer::MoveRight(float Value)
{
	AController* Controller = GetPawn()->Controller;
	if (Controller != nullptr && Value != 0.0f)
	{
		// Find out which way is forward
		const FRotator& Rotation = Controller->GetControlRotation();
		const FRotator YawRotation{ 0, Rotation.Yaw, 0 };

		const FVector Direction{ FRotationMatrix {YawRotation}.GetUnitAxis(EAxis::Y) };
		GetPawn()->AddMovementInput(Direction, Value);
	}
}

void UGKCharacterBrainPlayer::TurnAtRate(float Rate)
{
	GetSteering()->TurnAtRate(Rate);
}

void UGKCharacterBrainPlayer::LookUpAtRate(float Rate)
{
	GetSteering()->LookUpAtRate(Rate);
}

void UGKCharacterBrainPlayer::FireWeapon()
{
	GKLog("Fire Weapon.");
}