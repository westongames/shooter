// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GKWeaponTable.generated.h"

/*
GKWeaponTable will contain all of the possible weapon in the games. It will contains sound, mesh, etc.
GKWeapon is a simple struct and will point to GKWeaponTable, and contains other data like amount of bullet.
GKWeaponManager may own 0 or multiple GKWeapon.
*/

USTRUCT()
struct FWeaponInfo
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		int FireSoundCue;		// Which SoundCue to play for firing, refer to GKSoundManager

	UPROPERTY()
		float Throttle;
	UPROPERTY()
		float SteeringThrow;

	UPROPERTY()
		float DeltaTime;
	UPROPERTY()
		float Time;

	bool IsValid() const
	{
		return FMath::Abs(Throttle) <= 1 && FMath::Abs(SteeringThrow) <= 1;
	}
};

UCLASS()
class SHOOTER_API AGKWeaponTable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGKWeaponTable();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	TArray<FWeaponInfo> ArraySoundCues;
};
