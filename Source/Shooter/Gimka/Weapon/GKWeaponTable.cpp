// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Weapon/GKWeaponTable.h"

// Sets default values
AGKWeaponTable::AGKWeaponTable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGKWeaponTable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGKWeaponTable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

