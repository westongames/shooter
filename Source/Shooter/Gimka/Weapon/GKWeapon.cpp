// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Weapon/GKWeapon.h"
#include "Gimka/Weapon/GKWeaponManager.h"

#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

void UGKWeapon::FireWeapon(UGKWeaponManager* WeaponManager)
{
	if (FireSound)
	{
		UGameplayStatics::PlaySound2D(WeaponManager->GetOwner(), FireSound);
	}
}
