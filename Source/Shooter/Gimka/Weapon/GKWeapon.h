// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GKWeapon.generated.h"

/**
 *
 */
UCLASS()
class SHOOTER_API UGKWeapon : public UObject
{
	GENERATED_BODY()

	void FireWeapon(class UGKWeaponManager* WeaponManager);

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
	class USoundCue* FireSound;
};
