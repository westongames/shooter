// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Sound/GKSoundManager.h"

#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

AGKSoundManager* AGKSoundManager::Instance = nullptr;

// Sets default values
AGKSoundManager::AGKSoundManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Instance = this;
}

// Called when the game starts or when spawned
void AGKSoundManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGKSoundManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

