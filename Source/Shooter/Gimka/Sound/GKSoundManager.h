// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Gimka/Misc/GKHeader.h"

#include "GKSoundManager.generated.h"

/*
Contains all sounds that are in used for the level. Actors will have reference to this.
*/

UCLASS()
class SHOOTER_API UGKSoundInfo : public UObject
{
	GENERATED_BODY()

	UPROPERTY()
		float Throttle;
	UPROPERTY()
		float SteeringThrow;

	UPROPERTY()
		float DeltaTime;
	UPROPERTY()
		float Time;

	bool IsValid() const
	{
		return FMath::Abs(Throttle) <= 1 && FMath::Abs(SteeringThrow) <= 1;
	}
};

UCLASS()
class SHOOTER_API AGKSoundManager : public AActor
{
	GENERATED_BODY()
	
protected:
	AGKSoundManager();
	GKSINGLETON_ACTOR(AGKSoundManager)

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
	TArray<UGKSoundInfo*> ArraySoundInfos;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
	TArray<class USoundCue*> ArraySoundCues;
};
