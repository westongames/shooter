// Fill out your copyright notice in the Description page of Project Settings.


#include "GKCharacterSteering.h"

// Sets default values for this component's properties
UGKCharacterSteering::UGKCharacterSteering() :
	BaseTurnRate(45.f),
	BaseLookUpRate(45.f)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGKCharacterSteering::BeginPlay()
{
	Super::BeginPlay();

	// The Actor must be a Pawn
	Pawn = Cast<APawn>(GetOwner());
	check(Pawn);
}

void UGKCharacterSteering::TurnAtRate(float Rate)
{
	// Calculate delta for this frame from the rate information
	Pawn->AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());		// deg/sec * sec/frame
}


void UGKCharacterSteering::LookUpAtRate(float Rate)
{
	// Calculate delta for this frame from the rate information
	Pawn->AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());		// deg/sec * sec/frame
}

// Called every frame
void UGKCharacterSteering::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

