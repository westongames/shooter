// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "Gimka/Misc/GKHeader.h"

#include "GKCharacter.generated.h"

UCLASS()
class SHOOTER_API AGKCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AGKCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	// The controller
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Controller, meta = (AllowPrivateAccess = "true"))
	class UGKCharacterBrain* CharacterBrain;

	// The movement
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	class UGKCharacterSteering* CharacterSteering;

	// The weapon manager, might be null if not supported.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	class UGKWeaponManager* WeaponManager;

protected:
	GKPTR_SET(UGKCharacterBrain, CharacterBrain)
	GKPTR_SET(UGKCharacterSteering, CharacterSteering)
	GKPTR_SET(UGKWeaponManager, WeaponManager)
};
