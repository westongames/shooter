// Fill out your copyright notice in the Description page of Project Settings.


#include "GKCharacter.h"
#include "Gimka/AI/Brain/GKCharacterBrain.h"

// Sets default values
AGKCharacter::AGKCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AGKCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGKCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AGKCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(CharacterBrain);

	CharacterBrain->SetupInputComponent(PlayerInputComponent);
}

